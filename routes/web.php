<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/game/{id_user}/{id_room}', function(String $id_user, String $id_room){
    return view('game')->with('id_user', $id_user)->with('id_room', $id_room);
});

//Rutas para crear y listar las salas de juego
Route::resource('roomGame','RoomGameController');
Route::get('validationCode/{code}','RoomGameController@validationCode');