<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomGame extends Model
{
    protected $table = 'room_games';
    protected $guarded = ['id'];
    protected $fillable = [
        'room_name','code_game','id_user_creator','status'
    ];
}
