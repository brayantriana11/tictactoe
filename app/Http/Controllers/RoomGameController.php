<?php

namespace App\Http\Controllers;

use App\RoomGame;
use Illuminate\Http\Request;

class RoomGameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Se pagina la informacion de RoomGame para poder visualizar mejor la información

        $data = RoomGame::where('status',1)->orderBy('id','desc')->paginate(10);
        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $createRoom = new RoomGame();
        $createRoom->room_name = $request->room_name;
        $createRoom->code_game = $request->code_game;
        $createRoom->id_user_creator = $request->id_user_creator;
        $createRoom->status = 1;

        $createRoom->save();

        $id_game = RoomGame::latest('id')->first();

        return response()->json($id_game);
    }

    public function validationCode(String $code){
        $validationCode = RoomGame::where('code_game',$code)->where('status',1)->first();

        if(isset($validationCode)){
            return response()->json($validationCode);
        }else{
            return response()->json([]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RoomGame  $roomGame
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $showRoom = RoomGame::where('id',$id)->first();
        return response()->json($showRoom);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RoomGame  $roomGame
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updateRoom = RoomGame::findOrFail($id);
        $updateRoom->status = $request->status;
        $updateRoom->update();

        return response()->json('Sala Actualizada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RoomGame  $roomGame
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRoom = RoomGame::findOrFail($id);
        $deleteRoom->delete();

        return response()->json('Sala Eliminada');
    }
}
