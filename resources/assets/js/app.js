
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

window.Popper = require('popper.js').default;
require('./bootstrap');
require('axios');
window.toastr = require("toastr");
window.Vue = require('vue');
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('room-game-component', require('./components/RoomGameComponent.vue'));
Vue.component('create-room-component', require('./components/ModalCreateRoom.vue'));
Vue.component('game-component', require('./components/GameComponent.vue'));

const app = new Vue({
    el: '#app'
});
