@extends('layouts.app')

@section('content')
    <game-component :id_user="{{ $id_user }}" :id_room="{{ $id_room }}"></game-component>
@endsection