## versiones 

- php version 8.1.1
- npm version 6.14.11
- composer version 2.0.11 

## Instalacion
- composer install
- npm install
- php artisan migrate --seed

## Ejecución
- php artisan serve
- npm run watch

## usuarios de test
- Jugador 1

usuario: player1@email.com
Contraseña: 123456

- Jugador 2
usuario: player2@email.com
Contraseña: 123456